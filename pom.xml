<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!-- Inherited Icy Parent POM -->
    <parent>
        <groupId>org.bioimageanalysis.icy</groupId>
        <artifactId>parent-pom-plugin</artifactId>
        <version>1.0.6</version>
    </parent>

    <!-- Project Information -->
    <artifactId>nd4j</artifactId>
    <version>1.0.0</version>

    <packaging>jar</packaging>

    <name>Deeplearning4J</name>
    <description>Deeplearning4J library for Icy (https://deeplearning4j.konduit.ai/)</description>

    <url>http://icy.bioimageanalysis.org/plugin/deeplearning4j-for-icy/</url>
    <inceptionYear>2022</inceptionYear>

    <organization>
        <name>Institut Pasteur</name>
        <url>https://pasteur.fr</url>
    </organization>

    <licenses>
        <license>
            <name>GNU GPLv3</name>
            <url>https://www.gnu.org/licenses/gpl-3.0.en.html</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>sdallongeville</id>
            <name>Stéphane Dallongeville</name>
            <url>https://research.pasteur.fr/fr/member/stephane-dallongeville/</url>
            <roles>
                <role>founder</role>
                <role>lead</role>
                <role>architect</role>
                <role>developer</role>
                <role>debugger</role>
                <role>tester</role>
                <role>maintainer</role>
                <role>support</role>
            </roles>
        </developer>
    </developers>

    <!-- Project properties -->
    <properties>

    </properties>

    <!-- Project build configuration -->
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>${project.artifactId}-fetch</id>
                        <phase>generate-sources</phase>
                        <goals>
                            <goal>unpack-dependencies</goal>
                        </goals>
                        <configuration>
                            <includeArtifactIds>deeplearning4j-core,protobuf,jackson,nd4j-common,nd4j-presets-common,nd4j-api,nd4j-native,nd4j-native-api,nd4j-native-preset,guava</includeArtifactIds>
                            <outputDirectory>${project.build.outputDirectory}</outputDirectory>
                            <stripVersion>true</stripVersion>
                            <excludeTransitive>false</excludeTransitive>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <!-- List of project's dependencies -->
    <dependencies>
        <!-- The core of Icy -->
        <dependency>
            <groupId>org.bioimageanalysis.icy</groupId>
            <artifactId>icy-kernel</artifactId>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.deeplearning4j/deeplearning4j-core -->
        <dependency>
            <groupId>org.deeplearning4j</groupId>
            <artifactId>deeplearning4j-core</artifactId>
            <version>1.0.0-M2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.nd4j/protobuf -->
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>protobuf</artifactId>
            <version>1.0.0-M2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.nd4j/jackson -->
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>jackson</artifactId>
            <version>1.0.0-M2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.nd4j/nd4j-common -->
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>nd4j-common</artifactId>
            <version>1.0.0-M2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.nd4j/nd4j-presets-common -->
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>nd4j-presets-common</artifactId>
            <version>1.0.0-M2</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.nd4j/nd4j-api -->
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>nd4j-api</artifactId>
            <version>1.0.0-M2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.nd4j/nd4j-native -->
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>nd4j-native</artifactId>
            <version>1.0.0-M2</version>
            <scope>test</scope>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.nd4j/nd4j-native-api -->
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>nd4j-native-api</artifactId>
            <version>1.0.0-M2</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.nd4j/nd4j-native-preset -->
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>nd4j-native-preset</artifactId>
            <version>1.0.0-M2</version>
        </dependency>
        <dependency>
            <groupId>org.nd4j</groupId>
            <artifactId>guava</artifactId>
            <version>1.0.0-M2</version>
        </dependency>
    </dependencies>

    <!-- Icy Maven repository (to find parent POM) -->
    <repositories>
        <repository>
            <id>icy</id>
            <name>Icy's Nexus</name>
            <url>https://icy-nexus.pasteur.fr/repository/Icy/</url>
        </repository>
    </repositories>
</project>
