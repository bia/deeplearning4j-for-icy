package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the DeepLearning4J / NDJ4 library.
 * 
 * @author Stephane Dallongeville
 */
public class NDJ4LibraryPlugin extends Plugin implements PluginLibrary
{
    //
}
